import 'package:dartz/dartz.dart';
import 'package:portugal_iniciante/core/error/exception.dart';
import 'package:portugal_iniciante/core/error/failure.dart';
import 'package:portugal_iniciante/features/avantages/data/datasources/place_datasource.dart';
import 'package:portugal_iniciante/features/avantages/data/models/place_model.dart';
import 'package:portugal_iniciante/features/avantages/data/repositories/place_respository.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mockito/mockito.dart';

class MockPlaceDataSource extends Mock implements IPlaceDataSource {}

void main() {
  MockPlaceDataSource dataSource;
  PlaceRepository repository;

  final List<PlaceModel> tPlacesModel = [
    PlaceModel(
      conditions: <String>[],
      locations: <LatLng>[],
      name: "test",
      types: [],
      vicinity: "",
      icon: "",
      postalCode: "",
      locality: "",
      country: "",
      phoneNumber: "",
      fullAddress: "",
      url: "",
      district: "",
      address: "",
    )
  ];

  setUp(() {
    dataSource = MockPlaceDataSource();
    repository = PlaceRepository(dataSource: dataSource);
  });

  test('it should return [PlaceModel] from repository', () async {
    when(dataSource.getPlaces()).thenAnswer((_) async => tPlacesModel);

    final result = await repository.getPlaces();

    expect(result, Right(tPlacesModel));
    verify(dataSource.getPlaces());
    verifyNoMoreInteractions(dataSource);
  });

  test('it should return [ServerFailure] in case of a failure', () async {
    when(dataSource.getPlaces()).thenThrow(ServerException());

    final result = await repository.getPlaces();

    verify(dataSource.getPlaces());
    expect(result, Left(ServerFailure()));
    verifyNoMoreInteractions(dataSource);
  });
}
