import 'package:portugal_iniciante/core/error/exception.dart';
import 'package:portugal_iniciante/features/avantages/data/datasources/place_datasource.dart';
import 'package:portugal_iniciante/features/avantages/data/models/place_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mockito/mockito.dart';

import '../../../../mocks/firebase_mocks.dart';

void main() {
  MockFirestore mockFirestore;
  PlaceDataSourceFirestore dataSource;

  PlaceModel tContactModel = PlaceModel(
    conditions: <String>[],
    locations: <LatLng>[],
    types: <String>[],
    name: "",
    locality: "",
    postalCode: "",
    icon: "",
    vicinity: "",
    fullAddress: "",
    phoneNumber: "",
    country: "",
    address: "",
    url: "",
    district: "",
  );

  setUp(() {});

  group('getPlaces', () {
    test('should perform a request to the API endpoint to get places as json',
        () async {
      mockFirestore = MockFirestore();
      final mCollectionReference = MockCollectionReference();
      final mDocument = MockQueryDocumentSnapshot();
      final mSnapshot = MockQuerySnapshot();
      final mSnapshots = Future.value(mSnapshot);
      dataSource = PlaceDataSourceFirestore(firestore: mockFirestore);

      when(mockFirestore.collection("aplaces"))
          .thenReturn(mCollectionReference);
      when(mCollectionReference.get()).thenAnswer((_) => mSnapshots);
      when(mSnapshot.docs).thenReturn([mDocument]);
      when(mDocument.data()).thenReturn(tContactModel.toJson());

      final result = await dataSource.getPlaces();

      verify(mockFirestore.collection("aplaces"));
      verify(mCollectionReference.get());
      verify(mSnapshot.docs);
      expect(result, equals([tContactModel]));
    });

    test('should throw ServerException when it fails to get places', () async {
      mockFirestore = MockFirestore();
      dataSource = PlaceDataSourceFirestore(firestore: mockFirestore);
      when(mockFirestore.collection("")).thenThrow(Exception());

      final call = dataSource.getPlaces;

      expect(() => call(), throwsA(isA<ServerException>()));
    });
  });
}
