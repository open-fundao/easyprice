import 'dart:convert';

import 'package:portugal_iniciante/features/avantages/data/models/place_model.dart';
import 'package:portugal_iniciante/features/avantages/domain/entities/place.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../../../fixture/fixture_reader.dart';

main() {
  final tPlaceModel = PlaceModel(
    name: "Kredus Bar",
    conditions: ["10% em tudo", "Não aplicável a menus"],
    locations: [LatLng(40.1450272, -7.4993134)],
    phoneNumber: "",
    types: ["foo"],
    icon: "",
    vicinity: "",
    locality: "",
    country: "",
    postalCode: "",
    fullAddress: "",
    url: "",
    district: "",
    address: "",
  );

  test('should be a place', () async {
    expect(tPlaceModel, isA<Place>());
  });

  group("fromJson", () {
    test("should convert from json to PlaceModel", () async {
      final Map<String, dynamic> json = jsonDecode(fixture("place.json"));
      final result = PlaceModel.fromJson(json);
      expect(result, equals(tPlaceModel));
    });
  });

  group('toJson', () {
    test('should return a json from a model', () async {
      final Map<String, dynamic> json = tPlaceModel.toJson();
      final result = jsonDecode(fixture("place.json"));
      expect(json, equals(result));
    });
  });
}
