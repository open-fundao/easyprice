import 'package:dartz/dartz.dart';
import 'package:portugal_iniciante/features/avantages/domain/entities/place.dart';
import 'package:portugal_iniciante/features/avantages/domain/repositories/place_repository.dart';
import 'package:portugal_iniciante/features/avantages/domain/usecases/get_places.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mockito/mockito.dart';

class MockPlacesRepository extends Mock implements IPlaceRepository {}

void main() {
  MockPlacesRepository repository;
  GetPlaces getPlaces;

  final List<Place> tPlaces = [
    Place(
        conditions: <String>[],
        locations: <LatLng>[],
        name: "test",
        types: [],
        icon: "",
        vicinity: "",
        locality: "",
        postalCode: "",
        address: "",
        country: "",
        district: "",
        fullAddress: "",
        phoneNumber: "",
        url: "")
  ];

  setUp(() {
    repository = MockPlacesRepository();
    getPlaces = GetPlaces(repository: repository);
  });

  test("it should get a place list from repository", () async {
    when(repository.getPlaces()).thenAnswer((_) async => Right(tPlaces));

    final result = await getPlaces();
    expect(result, equals(Right(tPlaces)));
    verify(repository.getPlaces());
    verifyNoMoreInteractions(repository);
  });
}
