import 'package:dartz/dartz.dart';
import 'package:portugal_iniciante/core/error/failure.dart';
import 'package:portugal_iniciante/features/avantages/domain/entities/place.dart';
import 'package:portugal_iniciante/features/avantages/domain/usecases/get_places.dart';
import 'package:portugal_iniciante/features/avantages/presentation/bloc/bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mockito/mockito.dart';

class MockGetPlaces extends Mock implements GetPlaces {}

void main() {
  AvantagesBloc bloc;
  MockGetPlaces getPlaces;

  setUp(() {
    getPlaces = MockGetPlaces();
    bloc = AvantagesBloc(getPlaces: getPlaces);
  });

  test('inital state should be empty', () async {
    expect(bloc.initialState, equals(Empty()));
  });

  group('getPlaces', () {
    test('it should emmit [Loading,Loaded] if everything goes right', () async {
      when(getPlaces()).thenAnswer(
        (_) async => Right(
          [
            Place(
              conditions: <String>[],
              locations: <LatLng>[],
              address: "",
              country: "",
              district: "",
              fullAddress: "",
              icon: "",
              locality: "",
              phoneNumber: "",
              postalCode: "",
              url: "",
              vicinity: "",
              name: "",
              types: [],
            ),
          ],
        ),
      );
      final expected = [
        Empty(),
        Loading(),
        Loaded(places: [
          Place(
            conditions: <String>[],
            locations: <LatLng>[],
            address: "",
            country: "",
            district: "",
            fullAddress: "",
            icon: "",
            locality: "",
            phoneNumber: "",
            postalCode: "",
            url: "",
            vicinity: "",
            name: "",
            types: [],
          ),
        ]),
      ];

      expectLater(bloc, emitsInOrder(expected));

      bloc.add(GetAvantagesEvent());
    });

    test('it should emmit [Loading,Error] if something goes wrong', () async {
      when(getPlaces()).thenAnswer((_) async => Left(ServerFailure()));
      final expected = [
        Empty(),
        Loading(),
        Error(message: "Failed to fetch places"),
      ];

      expectLater(bloc, emitsInOrder(expected));

      bloc.add(GetAvantagesEvent());
    });
  });
}
