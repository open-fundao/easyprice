import 'package:dartz/dartz.dart';
import 'package:portugal_iniciante/core/error/failure.dart';
import 'package:portugal_iniciante/features/contacts/domain/entities/contact.dart';
import 'package:portugal_iniciante/features/contacts/domain/usecases/get_contacts.dart';
import 'package:portugal_iniciante/features/contacts/presentation/bloc/bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockGetContacts extends Mock implements GetContacts {}

void main() {
  ContactsBloc bloc;
  MockGetContacts getContacts;

  setUp(() {
    getContacts = MockGetContacts();
    bloc = ContactsBloc(getContacts: getContacts);
  });

  test('inital state should be empty', () async {
    expect(bloc.initialState, equals(Empty()));
  });

  group('getContacts', () {
    test('it should emmit [Loading,Loaded] if everything goes right', () async {
      when(getContacts()).thenAnswer(
        (_) async => Right(
          [
            Contact(category: "", description: "", number: "", title: ""),
          ],
        ),
      );
      final expected = [
        Empty(),
        Loading(),
        Loaded(contacts: [
          Contact(category: "", description: "", number: "", title: ""),
        ]),
      ];

      expectLater(bloc, emitsInOrder(expected));

      bloc.add(GetContactsEvent());
    });

    test('it should emmit [Loading,Error] if something goes wrong', () async {
      when(getContacts()).thenAnswer((_) async => Left(ServerFailure()));
      final expected = [
        Empty(),
        Loading(),
        Error(message: "Failed to fetch contacts"),
      ];

      expectLater(bloc, emitsInOrder(expected));

      bloc.add(GetContactsEvent());
    });
  });
}
