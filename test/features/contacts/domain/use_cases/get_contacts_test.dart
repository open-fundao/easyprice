import 'package:dartz/dartz.dart';
import 'package:portugal_iniciante/features/contacts/domain/entities/contact.dart';
import 'package:portugal_iniciante/features/contacts/domain/repositories/contact_repository.dart';
import 'package:portugal_iniciante/features/contacts/domain/usecases/get_contacts.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockContactsRepository extends Mock implements IContactsRepository {}

void main() {
  GetContacts getContacts;
  MockContactsRepository repository;

  setUp(() {
    repository = MockContactsRepository();
    getContacts = GetContacts(repository: repository);
  });

  final contacts = [
    Contact(
        category: "europe",
        description: "some number",
        number: "122",
        title: "Number"),
    Contact(
        category: "europe",
        description: "some number",
        number: "122",
        title: "Number")
  ];

  test('shoud get contacts from the repository', () async {
    when(repository.getContacts()).thenAnswer((_) async => Right(contacts));

    final result = await getContacts();
    expect(result, equals(Right(contacts)));

    verify(repository.getContacts());
    verifyNoMoreInteractions(repository);
  });
}
