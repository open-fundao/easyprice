import 'package:dartz/dartz.dart';
import 'package:portugal_iniciante/core/error/exception.dart';
import 'package:portugal_iniciante/core/error/failure.dart';
import 'package:portugal_iniciante/features/contacts/data/datasources/contact_datasource.dart';
import 'package:portugal_iniciante/features/contacts/data/models/contact_model.dart';
import 'package:portugal_iniciante/features/contacts/data/repositories/contact_repository.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockContactDataSource extends Mock implements IContactDataSource {}

void main() {
  MockContactDataSource mockDataSource = MockContactDataSource();
  ContactRepository repository = ContactRepository(dataSource: mockDataSource);

  group('getContacts', () {
    final tContactModels = [
      ContactModel(category: "", title: "", description: "", number: ""),
      ContactModel(category: "", title: "", description: "", number: ""),
    ];
    final tContacts = tContactModels;

    test('should get contacts from repository', () async {
      when(mockDataSource.getContacts())
          .thenAnswer((_) async => tContactModels);

      final result = await repository.getContacts();

      verify(mockDataSource.getContacts());
      expect(result, equals(Right(tContacts)));
      verifyNoMoreInteractions(mockDataSource);
    });

    test(
        'should return ServerFailure when it fails to get contacts from datasource',
        () async {
      when(mockDataSource.getContacts()).thenThrow(ServerException());

      final result = await repository.getContacts();
      verify(mockDataSource.getContacts());

      expect(result, Left(ServerFailure()));
      verifyNoMoreInteractions(mockDataSource);
    });
  });
}
