import 'package:portugal_iniciante/features/contacts/data/models/contact_model.dart';
import 'package:portugal_iniciante/features/contacts/domain/entities/contact.dart';
import 'package:flutter_test/flutter_test.dart';
import 'dart:convert';

import '../../../../fixture/fixture_reader.dart';

void main() {
  final tContactModel = ContactModel(
      category: "Europa",
      title: "Número Único Europeu",
      description:
          "Este numero pode ser usado em todos os paises da União Européia.\nFunciona para policia/bombeiros/emergencias",
      number: "122");

  test("should be a subclasss of Contact entity", () async {
    expect(tContactModel, isA<Contact>());
  });

  group('fromJson', () {
    test('should return a valid model with the contact json', () async {
      final Map<String, dynamic> json = jsonDecode(fixture('contact.json'));
      final result = ContactModel.fromJson(json);
      expect(result, equals(tContactModel));
    });
  });

  group('toJson', () {
    test('should return a json from a Model', () async {
      final Map<String, dynamic> json = tContactModel.toJson();
      final result = {
        "category": "Europa",
        "title": "Número Único Europeu",
        "description":
            "Este numero pode ser usado em todos os paises da União Européia.\nFunciona para policia/bombeiros/emergencias",
        "number": "122"
      };

      expect(result, equals(json));
    });
  });
}
