import 'package:portugal_iniciante/core/error/exception.dart';
import 'package:portugal_iniciante/features/contacts/data/datasources/contact_datasource.dart';
import 'package:portugal_iniciante/features/contacts/data/models/contact_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../../../mocks/firebase_mocks.dart';

void main() {
  MockFirestore mockFirestore;
  ContactDataSourceFirestore dataSource;

  ContactModel tContactModel =
      ContactModel(category: "", title: "", description: "", number: "");

  setUp(() {});

  group('getContacts', () {
    test('should perform a request to the API endpoint to get contacts as json',
        () async {
      mockFirestore = MockFirestore();
      final mCollectionReference = MockCollectionReference();
      final mDocument = MockQueryDocumentSnapshot();
      final mSnapshot = MockQuerySnapshot();
      final mSnapshots = Future.value(mSnapshot);
      dataSource = ContactDataSourceFirestore(firestore: mockFirestore);

      when(mockFirestore.collection("contacts"))
          .thenReturn(mCollectionReference);
      when(mCollectionReference.get()).thenAnswer((_) => mSnapshots);
      when(mSnapshot.docs).thenReturn([mDocument]);
      when(mDocument.data()).thenReturn(tContactModel.toJson());

      final result = await dataSource.getContacts();

      verify(mockFirestore.collection("contacts"));
      verify(mCollectionReference.get());
      verify(mSnapshot.docs);
      expect(result, equals([tContactModel]));
    });

    test('should throw ServerException when it fails to get contatcs',
        () async {
      mockFirestore = MockFirestore();
      dataSource = ContactDataSourceFirestore(firestore: mockFirestore);
      when(mockFirestore.collection("")).thenThrow(Exception());

      final call = dataSource.getContacts;

      expect(() => call(), throwsA(isA<ServerException>()));
    });
  });
}
