import 'dart:ui';

import 'package:portugal_iniciante/core/ui/widgets/card_home_widget.dart';
import 'package:portugal_iniciante/features/avantages/presentation/pages/avantages_page.dart';
import 'package:portugal_iniciante/features/contacts/presentation/pages/contacts_page.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          HeaderWidget(),
          CardHomeWidget(
            imagePath: "assets/images/telephone.jpg",
            title: "Contatos Importantes",
            description: "Lista de contactos importantes ou emergênciais",
            navigationPage: ContactsPage(),
          ),
          CardHomeWidget(
            imagePath: "assets/images/map_travel.jpg",
            title: "A-Vantages",
            description: "Mapa de benefícios e parcerias da Altran Portugal",
            navigationPage: AVantagesPage(),
          ),
//          CardHomeWidget(
//            imagePath: "assets/images/grocery_fruits.jpg",
//            title: "Mercado",
//            description:
//                "Busque preços de produtos e gerencie sua lista de compras",
//            navigationPage: MercadoPage(),
//          ),
        ],
      ),
    );
  }
}

class HeaderWidget extends StatelessWidget {
  const HeaderWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      maintainBottomViewPadding: false,
      child: Padding(
        padding: const EdgeInsets.only(left: 16.0, right: 16.0, top: 8.0),
        child: Container(
          width: double.infinity,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Flexible(
                flex: 3,
                child: Text(
                  "Portugal para\nIniciantes",
                  textAlign: TextAlign.right,
                  style: GoogleFonts.aBeeZee(
                      fontWeight: FontWeight.w900,
                      fontSize: 32,
                      color: Colors.black),
                ),
              ),
              Flexible(
                flex: 1,
                child: Image.asset(
                  "assets/images/app-icon.png",
                  isAntiAlias: true,
                  fit: BoxFit.scaleDown,
                  width: 120,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
