import 'package:flutter/material.dart';
import 'dart:math';

import 'package:google_fonts/google_fonts.dart';

class GroceryListPage extends StatelessWidget {
  const GroceryListPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        centerTitle: true,
        backgroundColor: Theme.of(context).primaryColor,
        title: Text(
          'Churras da Galera',
          style: TextStyle(color: Theme.of(context).backgroundColor),
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: const Radius.circular(21),
          ),
        ),
        iconTheme: IconThemeData(color: Theme.of(context).backgroundColor),
      ),
      body: Center(child: Text('Body')),
    );
  }
}

class ListsPage extends StatelessWidget {
  final listas = List<String>.generate(10000, (i) => "Item $i");

  @override
  Widget build(BuildContext context) {
    final makeListTile = Container(
      height: 150,
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        borderRadius: BorderRadius.circular(21.0),
      ),
      child: Stack(children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 20, left: 20),
          child: Row(
            children: <Widget>[
              Text('Churras da Galera',
                  style: GoogleFonts.robotoSlab(
                    color: Theme.of(context).primaryColor,
                    fontWeight: FontWeight.bold,
                  )),
            ],
          ),
        ),
        Positioned(
          right: 0,
          top: -25,
          child: Transform.rotate(
            angle: 27 * pi / 180,
            child: Icon(
              Icons.shopping_basket,
              color: Theme.of(context).primaryColor.withOpacity(0.02),
              size: 180,
            ),
          ),
        )
      ]),
    );

    final makeCard = InkWell(
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(21.0),
        ),
        elevation: 5,
        margin: new EdgeInsets.symmetric(horizontal: 9.0, vertical: 6.0),
        child: makeListTile,
      ),
    );

    final makeBody = Container(
      child: ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: 10,
        itemBuilder: (BuildContext context, int index) {
          return makeCard;
        },
      ),
    );
    return makeBody;
  }
}
