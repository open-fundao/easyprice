import 'package:flutter/material.dart';

class NewListPage extends StatelessWidget {
  const NewListPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          elevation: 1,
          centerTitle: true,
          backgroundColor: Theme.of(context).primaryColor,
          title: Text(
            "Create a new list",
            style: TextStyle(color: Theme.of(context).backgroundColor),
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
              bottom: const Radius.circular(21),
            ),
          ),
          iconTheme: IconThemeData(color: Theme.of(context).backgroundColor)),
      body: Container(
        child: SingleChildScrollView(
          child: Column(children: <Widget>[
            ListTile(
              leading: const Icon(Icons.list),
              title: TextField(
                decoration: InputDecoration(
                  hintText: "List name",
                ),
              ),
            ),
            ListTile(
              leading: const Icon(Icons.comment),
              title: new TextField(
                decoration: InputDecoration(
                  hintText: "Description",
                ),
              ),
            ),
          ]),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.save_alt,
          color: Theme.of(context).primaryColor,
        ),
        onPressed: () {},
        backgroundColor: Theme.of(context).accentColor,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
