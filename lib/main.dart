import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';

import 'package:flutter/material.dart';
import 'package:portugal_iniciante/core/ui/pages/home_page.dart';
import 'package:portugal_iniciante/features/avantages/data/datasources/place_datasource.dart';
import 'package:portugal_iniciante/features/avantages/data/repositories/place_respository.dart';
import 'package:portugal_iniciante/features/avantages/domain/usecases/get_places.dart';
import 'package:portugal_iniciante/features/avantages/presentation/bloc/avantages_bloc.dart';
import 'package:portugal_iniciante/features/contacts/data/datasources/contact_datasource.dart';
import 'package:portugal_iniciante/features/contacts/data/repositories/contact_repository.dart';
import 'package:portugal_iniciante/features/contacts/domain/usecases/get_contacts.dart';
import 'package:portugal_iniciante/features/contacts/presentation/bloc/contacts_bloc.dart';
import 'package:nested/nested.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(App());
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: providers(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Fundão para Iniciantes',
        theme: ThemeData(
          // Define the default brightness and colors.
          brightness: Brightness.light,
          backgroundColor: Colors.grey[200],
          primaryColor: Colors.lightBlue[800],
          accentColor: Colors.cyan[600],

          // Define the default font family.
          //fontFamily: 'Georgia',

          // Define the default TextTheme. Use this to specify the default
          // text styling for headlines, titles, bodies of text, and more.
          //textTheme: TextTheme(
          //  headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
          //  headline6: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
          //  bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
          //),
        ),
        home: MainPage(),
      ),
    );
  }

  List<SingleChildWidget> providers() {
    return [
      ChangeNotifierProvider<ContactsBloc>(
        create: (_) {
          var dataSource =
              ContactDataSourceFirestore(firestore: FirebaseFirestore.instance);
          var repository = ContactRepository(dataSource: dataSource);
          var contacts = GetContacts(repository: repository);
          return ContactsBloc(getContacts: contacts);
        },
      ),
      ChangeNotifierProvider(
        create: (_) {
          PlaceDataSourceFirestore dataSource =
              PlaceDataSourceFirestore(firestore: FirebaseFirestore.instance);
          PlaceRepository repository = PlaceRepository(dataSource: dataSource);
          GetPlaces places = GetPlaces(repository: repository);
          return AvantagesBloc(getPlaces: places);
        },
      ),
    ];
  }
}

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: HomePage(),
    );
  }
}
