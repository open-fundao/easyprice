import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:portugal_iniciante/features/avantages/domain/entities/place.dart';
import 'package:portugal_iniciante/features/avantages/presentation/widgets/details_widget.dart';

class PlaceDetailsPage extends StatelessWidget {
  final Place place;

  const PlaceDetailsPage({Key key, @required this.place}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        elevation: 0,
        title: Text(
          place.name,
          style: GoogleFonts.aBeeZee(
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
        ),
        backgroundColor: Theme.of(context).backgroundColor,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          DetailsWidget(
            title: "Condições",
            details: this.place.conditions,
          ),
          ContactDetailsWidget(
            colors: [Colors.orange, Colors.deepOrangeAccent[400]],
            place: place,
          ),
        ],
      ),
    );
  }
}
