import 'dart:async';
import 'dart:ui';

import 'package:after_layout/after_layout.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:portugal_iniciante/features/avantages/data/models/place_model.dart';
import 'package:portugal_iniciante/features/avantages/presentation/bloc/avantages_bloc.dart';
import 'package:portugal_iniciante/features/avantages/presentation/bloc/avantages_event.dart';
import 'package:portugal_iniciante/features/avantages/presentation/bloc/avantages_state.dart';
import 'package:portugal_iniciante/features/avantages/presentation/pages/place_details_page.dart';
import 'package:portugal_iniciante/features/avantages/presentation/widgets/place_card_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';

class AVantagesPage extends StatefulWidget {
  const AVantagesPage({Key key}) : super(key: key);

  @override
  _AVantagesPageState createState() => _AVantagesPageState();
}

class _AVantagesPageState extends State<AVantagesPage>
    with SingleTickerProviderStateMixin, AfterLayoutMixin<AVantagesPage> {
  // good picture of entire Portugal
  final CameraPosition _kPortugal =
      CameraPosition(target: LatLng(40, -7.8), zoom: 7.0);

  final Completer<GoogleMapController> _controller = Completer();
  final Geolocator _locator = Geolocator();
  final _bottomCardsController = ScrollController();

  final Map<PlaceModel, Set<Marker>> placesAndMarkers = {};

  bool _shouldShowCards = false;

  AvantagesState oldState;
  Marker selectedMarker;
  PlaceModel selectedPlace;
  MarkerId selectedMarkerId;

  List<Widget> placesWidgets = [];

  ///
  /// Move camera to location
  Future goToLocation() async {
    final location = await getCurrentLocation();
    final controller = await _controller.future;
    controller.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(location.latitude, location.longitude),
          zoom: 18.0,
        ),
      ),
    );
  }

  Future<Position> getCurrentLocation() async {
    var location =
        _locator.getLastKnownPosition(desiredAccuracy: LocationAccuracy.best);
    if (location == null) {
      location = _locator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.best,
      );
    }
    return location;
  }

  void updateMarkers(List<PlaceModel> places) {
    //@todo: find a more elegant way of doing this
    for (PlaceModel place in places) {
      final markers = place.locations.map<Marker>(
        (location) {
          final markerId = MarkerId("$location");
          return Marker(
            markerId: markerId,
            position: location,
            visible: true,
            consumeTapEvents: true,
            icon: BitmapDescriptor.defaultMarker,
            onTap: () async {
              setState(() => _shouldShowCards = true);
              moveCameraToLocation(location);
              placesAndMarkers.forEach((place, markers) {
                if (markers.any((m) => m.markerId == markerId)) {
                  setState(() => selectedPlace = place);
                  return;
                }
              });

              final keys = placesAndMarkers.keys.toList();
              final index = keys.indexOf(selectedPlace);
              _bottomCardsController.jumpTo(index * 320.0 + index * 16 - 12);
            },
          );
        },
      ).toSet();
      placesAndMarkers.putIfAbsent(place, () => markers);
    }
  }

  Future moveCameraToLocation(LatLng location) async {
    final controller = await _controller.future;
    controller.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(target: location, zoom: 20.0),
      ),
    );
  }

  @override
  void dispose() {
    _bottomCardsController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _bottomCardsController.addListener(() {
      final scrollPos = _bottomCardsController.position;
      final index = (scrollPos.pixels / (320 + 16)).round();
      final keys = placesAndMarkers.keys.toList();
      final place = keys[index];

      //@note: what if there is multiple places?
      moveCameraToLocation(place.locations.first);
    });
  }

  @override
  void afterFirstLayout(BuildContext context) {
    Provider.of<AvantagesBloc>(context, listen: false).add(GetAvantagesEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Consumer<AvantagesBloc>(builder: (_, bloc, __) {
        final blocState = bloc.state;

        List<PlaceModel> places = [];
        if (blocState is Loaded && blocState != oldState) {
          oldState = blocState;
          places = blocState.places;
          this.placesWidgets = places
              .map((place) => Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: PlaceCardWidget(place: place)))
              .toList(growable: false);
          updateMarkers(places);
        }

        final Set<Marker> markers =
            placesAndMarkers.values.expand((m) => m).toSet();

        final map = GoogleMap(
          mapType: MapType.normal,
          initialCameraPosition: _kPortugal,
          onMapCreated: (GoogleMapController controller) {
            _controller.complete(controller);
          },
          zoomControlsEnabled: false,
          compassEnabled: false,
          circles: {},
          markers: markers,
          myLocationEnabled: true,
          myLocationButtonEnabled: false,
          indoorViewEnabled: false,
        );

        if (blocState is Error) {
          debugPrint("Is error: ${blocState.message}");
          return Stack(children: [
            map,
            Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              Center(
                child: AlertDialog(
                  backgroundColor: Colors.red,
                  elevation: 8,
                  title: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: Icon(Icons.error, color: Colors.white),
                      ),
                      Text(
                        "Atenção",
                        style: GoogleFonts.aBeeZee(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                  content: Text(
                    "Ocorreu uma falha ao carregar os lugares com promoções.",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.aBeeZee(
                      fontSize: 14,
                      color: Colors.white,
                    ),
                  ),
                  actions: [
                    FlatButton(
                      onPressed: () => Navigator.pop(context),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(16),
                      ),
                      child: Text(
                        "Voltar",
                        style: GoogleFonts.aBeeZee(color: Colors.red),
                      ),
                      color: Colors.white,
                    )
                  ],
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16)),
                ),
              ),
            ])
          ]);
        }

        if (blocState is Loading) {
          return Stack(
            children: [
              map,
              Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [Center(child: RefreshProgressIndicator())])
            ],
          );
        }

        return Stack(
          children: <Widget>[
            map,
            buildHeader(context),
            buildBottomCards(context),
            Positioned(
              bottom: _shouldShowCards ? 184 : 32,
              right: 16,
              child: FloatingActionButton(
                mini: true,
                elevation: 2,
                backgroundColor: Colors.white,
                onPressed: goToLocation,
                child: Icon(Icons.my_location, color: Colors.blue),
              ),
            ),
          ],
        );
      }),
    );
  }

  SafeArea buildHeader(BuildContext context) {
    return SafeArea(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        blurRadius: 8, spreadRadius: -8, offset: Offset(0, 3))
                  ]),
              height: 50,
              child: AppBar(
                backgroundColor: Colors.white,
                iconTheme: Theme.of(context).iconTheme,
                primary: false,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                title: TextField(
                  onTap: () {
                    return Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => SearchPlacesPage(
                            places: this.placesAndMarkers.keys.toList()),
                      ),
                    );
                  },
                  textAlignVertical: TextAlignVertical.center,
                  maxLines: 1,
                  decoration: InputDecoration(
                    hintText: "Procurar um local",
                    hintStyle: GoogleFonts.aBeeZee(),
                    contentPadding: EdgeInsets.only(left: 15, top: 0),
                    suffixIcon: Icon(
                      Icons.search,
                      color: Colors.black,
                    ),
                    border: InputBorder.none,
                  ),
                ),
              ),
            ),
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: ChoiceChip(
                      backgroundColor: Colors.white,
                      label: Text('Animals'),
                      avatar: FaIcon(FontAwesomeIcons.paw, size: 18),
                      selected: false),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: ChoiceChip(
                      backgroundColor: Colors.white,
                      label: Text('Beauty & Well-Being'),
                      avatar: FaIcon(FontAwesomeIcons.spa, size: 18),
                      selected: false),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: ChoiceChip(
                      backgroundColor: Colors.white,
                      label: Text('Education & Training'),
                      avatar: FaIcon(FontAwesomeIcons.school, size: 18),
                      selected: false),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: ChoiceChip(
                      backgroundColor: Colors.white,
                      label: Text('Health'),
                      avatar: FaIcon(FontAwesomeIcons.userMd, size: 18),
                      selected: false),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: ChoiceChip(
                      backgroundColor: Colors.white,
                      label: Text('Hotels & Tourism'),
                      avatar: FaIcon(FontAwesomeIcons.hotel, size: 18),
                      selected: false),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: ChoiceChip(
                      backgroundColor: Colors.white,
                      label: Text('Kids'),
                      avatar: FaIcon(FontAwesomeIcons.child, size: 18),
                      selected: false),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: ChoiceChip(
                      backgroundColor: Colors.white,
                      label: Text('Culture'),
                      avatar: FaIcon(FontAwesomeIcons.theaterMasks, size: 18),
                      selected: false),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: ChoiceChip(
                      backgroundColor: Colors.white,
                      label: Text('Food'),
                      avatar: FaIcon(FontAwesomeIcons.hamburger, size: 18),
                      selected: false),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: ChoiceChip(
                      backgroundColor: Colors.white,
                      label: Text('Sports & Gym'),
                      avatar: FaIcon(FontAwesomeIcons.dumbbell, size: 18),
                      selected: false),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: ChoiceChip(
                      backgroundColor: Colors.white,
                      label: Text('Stores & Services'),
                      avatar: FaIcon(FontAwesomeIcons.store, size: 18),
                      selected: false),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget buildBottomCards(BuildContext context) {
    return AnimatedPositioned(
      curve: Curves.easeIn,
      bottom: _shouldShowCards ? 40 : -136,
      duration: Duration(milliseconds: 200),
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 136,
        child: ListView(
          controller: _bottomCardsController,
          scrollDirection: Axis.horizontal,
          physics: ClampingScrollPhysics(),
          children: this.placesWidgets,
        ),
      ),
    );
  }
}

class SearchPlacesPage extends StatefulWidget {
  final List<PlaceModel> places;

  SearchPlacesPage({this.places = const []});

  @override
  _SearchPlacesPageState createState() =>
      _SearchPlacesPageState(places: this.places);
}

class _SearchPlacesPageState extends State<SearchPlacesPage> {
  final List<PlaceModel> places;
  final TextEditingController _textController = TextEditingController();
  List<PlaceModel> filtered = [];

  _SearchPlacesPageState({@required this.places});

  @override
  void initState() {
    super.initState();
    this.filtered.addAll(this.places);
  }

  @override
  void dispose() {
    _textController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        appBar: AppBar(
          elevation: 0,
          title: TextField(
            textAlignVertical: TextAlignVertical.center,
            maxLines: 1,
            decoration: InputDecoration(
              hintText: "Procurar um local",
              hintStyle: GoogleFonts.aBeeZee(),
              contentPadding: EdgeInsets.only(left: 15, top: 0),
              suffixIcon: Icon(
                Icons.search,
                color: Colors.black,
              ),
              border: InputBorder.none,
            ),
            onChanged: (newText) {
              setState(() {
                filtered = this
                    .places
                    .where((place) => place.name
                        .toLowerCase()
                        .contains(newText.toLowerCase()))
                    .toList();
              });
            },
          ),
          backgroundColor: Theme.of(context).backgroundColor,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Flexible(
              child: ListView.builder(
                  itemCount: filtered.length,
                  itemBuilder: (context, index) => Padding(
                        padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                        child: ListTile(
                          trailing: Icon(Icons.navigate_next),
                          contentPadding:
                              const EdgeInsets.symmetric(vertical: 8.0),
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) =>
                                    PlaceDetailsPage(place: filtered[index])));
                          },
                          title: Text(
                            filtered[index].name,
                            style: GoogleFonts.aBeeZee(),
                          ),
                        ),
                      )),
            )
          ],
        ));
  }
}
