import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:portugal_iniciante/features/avantages/domain/entities/place.dart';

class DetailsWidget extends StatelessWidget {
  final String title;
  final List<String> details;
  final List<Color> colors;

  const DetailsWidget({
    Key key,
    @required this.title,
    this.details = const <String>[],
    this.colors = const <Color>[],
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var widgetDetails = this
        .details
        .map<Widget>(
          (d) => Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child:
                      Icon(Icons.info_outline, color: Colors.white, size: 18),
                ),
                Flexible(
                  child: SelectableText(
                    d,
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ],
            ),
          ),
        )
        .toList();
    if (widgetDetails.isEmpty) {
      final noInfoWidget = Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: Icon(Icons.error, color: Colors.white, size: 18),
            ),
            Flexible(
              child: SelectableText(
                "Ainda não existem detalhes sobre ${title.toLowerCase()}",
                style: TextStyle(color: Colors.white),
              ),
            ),
          ],
        ),
      );

      widgetDetails.add(noInfoWidget);
    }

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
          gradient: LinearGradient(
            begin: FractionalOffset.topCenter,
            end: FractionalOffset.bottomCenter,
            colors: this.colors.isNotEmpty
                ? this.colors
                : [
                    Colors.blue.withOpacity(1.0),
                    Colors.deepPurple[300].withOpacity(1.0),
                  ],
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Text(
                  this.title,
                  style: GoogleFonts.aBeeZee(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 22),
                ),
              ),
            ]..addAll(widgetDetails),
          ),
        ),
      ),
    );
  }
}

class ContactDetailsWidget extends StatelessWidget {
  final String title = "Informações de Contato";
  final Place place;
  final List<Color> colors;

  const ContactDetailsWidget({
    Key key,
    @required this.place,
    this.colors = const <Color>[],
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final widgetDetails = <Widget>[
      RowDetailsWidget(
        detail: place.phoneNumber,
        icon: Icon(Icons.phone, color: Colors.white, size: 18),
      ),
      if (place.vicinity != null && place.vicinity.isNotEmpty)
        RowDetailsWidget(
          detail: place.vicinity,
          icon: Icon(Icons.place, color: Colors.white, size: 18),
        ),
      RowDetailsWidget(
        detail: place.postalCode,
        icon: Icon(Icons.mail, color: Colors.white, size: 18),
      ),
    ];

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
          gradient: LinearGradient(
            begin: FractionalOffset.topCenter,
            end: FractionalOffset.bottomCenter,
            colors: this.colors.isNotEmpty
                ? this.colors
                : [
                    Colors.blue.withOpacity(1.0),
                    Colors.deepPurple[300].withOpacity(1.0),
                  ],
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Text(
                  this.title,
                  style: GoogleFonts.aBeeZee(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 22),
                ),
              ),
            ]..addAll(widgetDetails),
          ),
        ),
      ),
    );
  }
}

class RowDetailsWidget extends StatelessWidget {
  RowDetailsWidget({
    Key key,
    @required this.detail,
    @required this.icon,
    this.onTap,
  }) : super(key: key);

  final String detail;
  final Icon icon;
  final GestureTapCallback onTap;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: InkWell(
        onTap: onTap,
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: icon,
            ),
            Flexible(
              child: SelectableText(
                this.detail,
                style: TextStyle(color: Colors.white),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
