import 'package:portugal_iniciante/features/avantages/domain/entities/place.dart';
import 'package:portugal_iniciante/features/avantages/presentation/pages/place_details_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class PlaceCardWidget extends StatelessWidget {
  const PlaceCardWidget({
    Key key,
    @required this.place,
  }) : super(key: key);

  final Place place;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => PlaceDetailsPage(place: place),
        ),
      ),
      child: Container(
        width: 320,
        decoration: BoxDecoration(
          color: Colors.black,
          borderRadius: BorderRadius.circular(8),
          gradient: LinearGradient(
            begin: FractionalOffset.topCenter,
            end: FractionalOffset.bottomCenter,
            colors: [
              Colors.blue.withOpacity(1.0),
              Colors.deepPurple[300].withOpacity(1.0),
            ],
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.place,
              color: Colors.white,
            ),
            Flexible(
              child: Text(
                place.name,
                overflow: TextOverflow.clip,
                style: TextStyle(color: Colors.white),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
