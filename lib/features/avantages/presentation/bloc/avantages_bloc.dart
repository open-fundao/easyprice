import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:portugal_iniciante/features/avantages/domain/usecases/get_places.dart';
import 'package:flutter/widgets.dart';
import './bloc.dart';

import 'package:meta/meta.dart';

class AvantagesBloc extends Bloc<AvantagesEvent, AvantagesState>
    with ChangeNotifier {
  final GetPlaces getPlaces;

  AvantagesBloc({@required this.getPlaces});

  @override
  AvantagesState get initialState => Empty();

  @override
  Stream<AvantagesState> mapEventToState(
    AvantagesEvent event,
  ) async* {
    if (event is GetAvantagesEvent) {
      final placesOrFailure = getPlaces();
      notifyListeners();
      yield Loading();
      final places = await placesOrFailure;

      notifyListeners();
      yield* places.fold((failure) async* {
        yield Error(message: "Failed to fetch places");
      }, (places) async* {
        yield Loaded(places: places);
      });
    }
  }
}
