import 'package:equatable/equatable.dart';

abstract class AvantagesEvent extends Equatable {
  const AvantagesEvent();
}

class GetAvantagesEvent extends AvantagesEvent {
  @override
  List<Object> get props => [];
}
