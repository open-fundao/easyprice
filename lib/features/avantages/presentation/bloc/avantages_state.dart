import 'package:portugal_iniciante/features/avantages/domain/entities/place.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class AvantagesState extends Equatable {
  const AvantagesState();
}

class Empty extends AvantagesState {
  @override
  List<Object> get props => [];
}

class Loading extends AvantagesState {
  @override
  List<Object> get props => [];
}

class Loaded extends AvantagesState {
  final List<Place> places;

  Loaded({@required this.places});

  @override
  List<Object> get props => [this.places];
}

class Error extends AvantagesState {
  final String message;

  Error({@required this.message});

  @override
  List<Object> get props => [this.message];
}
