import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class Place extends Equatable {
  final String name;
  final String address;
  final String country;
  final String district;
  final String fullAddress;
  final String icon;
  final String locality;
  final String phoneNumber;
  final String postalCode;
  final String url;
  final String vicinity;

  final List<LatLng> locations;
  final List<String> conditions;
  final List<String> types;

  Place({
    @required this.name,
    @required this.address,
    @required this.country,
    @required this.district,
    @required this.fullAddress,
    @required this.icon,
    @required this.locality,
    @required this.phoneNumber,
    @required this.postalCode,
    @required this.url,
    @required this.vicinity,
    @required this.locations,
    @required this.conditions,
    @required this.types,
  });

  @override
  List<Object> get props => [
        name,
        address,
        country,
        district,
        fullAddress,
        icon,
        locality,
        phoneNumber,
        postalCode,
        url,
        vicinity,
        locations,
        conditions,
        types
      ];
}
