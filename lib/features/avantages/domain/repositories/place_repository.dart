import 'package:dartz/dartz.dart';
import 'package:portugal_iniciante/core/error/failure.dart';
import 'package:portugal_iniciante/features/avantages/domain/entities/place.dart';

abstract class IPlaceRepository {
  Future<Either<Failure, List<Place>>> getPlaces();
}
