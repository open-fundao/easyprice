import 'package:dartz/dartz.dart';
import 'package:portugal_iniciante/core/error/failure.dart';
import 'package:portugal_iniciante/features/avantages/domain/entities/place.dart';
import 'package:portugal_iniciante/features/avantages/domain/repositories/place_repository.dart';
import 'package:meta/meta.dart';

class GetPlaces {
  final IPlaceRepository repository;

  GetPlaces({@required this.repository});

  Future<Either<Failure, List<Place>>> call() async {
    return await this.repository.getPlaces();
  }
}
