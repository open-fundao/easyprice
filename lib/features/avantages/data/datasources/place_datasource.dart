import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:portugal_iniciante/core/error/exception.dart';
import 'package:portugal_iniciante/features/avantages/data/models/place_model.dart';
import 'package:meta/meta.dart';
import 'dart:core';

abstract class IPlaceDataSource {
  /// Calls remote endpoint
  ///
  /// Throws a [ServerException] for all error codes
  Future<List<PlaceModel>> getPlaces();
}

class PlaceDataSourceFirestore implements IPlaceDataSource {
  final FirebaseFirestore firestore;

  PlaceDataSourceFirestore({@required this.firestore});

  @override
  Future<List<PlaceModel>> getPlaces() async {
    final List<PlaceModel> places = [];
    try {
      final documents = await firestore.collection("aplaces").get();

      places.addAll(documents.docs
          .map((document) => PlaceModel.fromJson(document.data())));
    } catch (exception) {
      throw ServerException();
    }
    return places;
  }
}
