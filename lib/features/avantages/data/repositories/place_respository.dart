import 'package:dartz/dartz.dart';
import 'package:portugal_iniciante/core/error/failure.dart';
import 'package:portugal_iniciante/features/avantages/data/datasources/place_datasource.dart';
import 'package:portugal_iniciante/features/avantages/domain/entities/place.dart';
import 'package:portugal_iniciante/features/avantages/domain/repositories/place_repository.dart';
import 'package:meta/meta.dart';

class PlaceRepository implements IPlaceRepository {
  final IPlaceDataSource dataSource;

  PlaceRepository({@required this.dataSource});
  @override
  Future<Either<Failure, List<Place>>> getPlaces() async {
    try {
      final List<Place> places = await dataSource.getPlaces();
      return Right(places);
    } on Exception {
      return Left(ServerFailure());
    }
  }
}
