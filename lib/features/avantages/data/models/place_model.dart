import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:portugal_iniciante/features/avantages/domain/entities/place.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:meta/meta.dart';

class PlaceModel extends Place {
  final String name;
  final String address;
  final String country;
  final String district;
  final String fullAddress;
  final String icon;
  final String locality;
  final String phoneNumber;
  final String postalCode;
  final String url;
  final String vicinity;

  final List<LatLng> locations;
  final List<String> conditions;
  final List<String> types;

  PlaceModel({
    @required this.name,
    @required this.address,
    @required this.country,
    @required this.district,
    @required this.fullAddress,
    @required this.icon,
    @required this.locality,
    @required this.phoneNumber,
    @required this.postalCode,
    @required this.url,
    @required this.vicinity,
    @required this.locations,
    @required this.conditions,
    @required this.types,
  }) : super(
            name: name,
            address: address,
            country: country,
            district: district,
            fullAddress: fullAddress,
            icon: icon,
            locality: locality,
            phoneNumber: phoneNumber,
            postalCode: postalCode,
            url: url,
            vicinity: vicinity,
            locations: locations,
            conditions: conditions,
            types: types);

  factory PlaceModel.fromJson(Map<String, dynamic> json) {
    final List<String> conditions = List<String>.from(json['conditions']);
    final List<String> types = List<String>.from(json['types']);

    List<LatLng> locations;
    try {
      final locationsMap = List<GeoPoint>.from(json['locations']);
      locations = locationsMap
          .map((location) => LatLng(location.latitude, location.longitude))
          .toList();
    } catch (_) {
      final locationsMap = List<Map<String, dynamic>>.from(json['locations']);
      locations = locationsMap
          .map(
              (location) => LatLng(location['latitude'], location['longitude']))
          .toList();
    }

    return PlaceModel(
      name: json['name'],
      conditions: conditions,
      locations: locations,
      types: types,
      address: json['address'],
      url: json['url'],
      phoneNumber: json['phone_number'],
      fullAddress: json['full_address'],
      district: json['district'],
      country: json['country'],
      icon: json['icon'],
      locality: json['locality'],
      postalCode: json['postal_code'],
      vicinity: json['vicinity'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "name": name,
      "locations": locations.map((location) => {
            // @note: we need to check if this can be done in a better way
            "latitude": double.parse(location.latitude.toStringAsFixed(7)),
            "longitude": double.parse(location.longitude.toStringAsFixed(7)),
          }),
      "conditions": conditions,
      "types": types,
      "address": address,
      "url": url,
      "phone_number": phoneNumber,
      "full_address": fullAddress,
      "district": district,
      "icon": icon,
      "locality": locality,
      "postal_code": postalCode,
      "vicinity": vicinity,
      "country": country
    };
  }

  @override
  String toString() {
    return 'PlaceModel{name: $name, locations: $locations, '
        'conditions: $conditions, types: $types, address: $address, '
        'url: $url, phoneNumber:$phoneNumber, fullAddress: $fullAddress,'
        'district: $district, icon: $icon, locality: $locality, '
        'postalCode: $postalCode, vicinity: $vicinity}';
  }
}
