import 'package:dartz/dartz.dart';
import 'package:portugal_iniciante/core/error/failure.dart';
import 'package:portugal_iniciante/features/contacts/domain/entities/contact.dart';
import 'package:portugal_iniciante/features/contacts/domain/repositories/contact_repository.dart';
import 'package:meta/meta.dart';

class GetContacts {
  final IContactsRepository repository;

  GetContacts({@required this.repository});

  Future<Either<Failure, List<Contact>>> call() async {
    return await this.repository.getContacts();
  }
}
