import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

class Contact extends Equatable {
  final String category;
  final String title;
  final String description;
  final String number;

  Contact({
    @required this.category,
    @required this.title,
    @required this.description,
    @required this.number,
  }) : super();

  @override
  List<Object> get props =>
      [this.category, this.title, this.description, this.number];
}
