import 'package:dartz/dartz.dart';
import 'package:portugal_iniciante/core/error/failure.dart';
import 'package:portugal_iniciante/features/contacts/domain/entities/contact.dart';

abstract class IContactsRepository {
  Future<Either<Failure, List<Contact>>> getContacts();
}
