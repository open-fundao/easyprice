import 'package:portugal_iniciante/features/contacts/domain/entities/contact.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactWidget extends StatelessWidget {
  const ContactWidget({
    Key key,
    @required this.contact,
  }) : super(key: key);

  final Contact contact;

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
        child: Container(
            decoration: BoxDecoration(
                color: Colors.black,
                borderRadius: BorderRadius.circular(8),
                gradient: LinearGradient(
                    begin: FractionalOffset.topCenter,
                    end: FractionalOffset.bottomCenter,
                    colors: [
                      Colors.purple.withOpacity(1.0),
                      Colors.purple[800].withOpacity(1.0),
                    ])),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(contact.title,
                      style: GoogleFonts.aBeeZee(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      )),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: Text(
                      contact.description,
                      style: GoogleFonts.aBeeZee(
                        fontStyle: FontStyle.italic,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Row(
                    children: [
                      IconButton(
                        icon: Icon(Icons.phone),
                        color: Colors.white,
                        onPressed: () async {
                          if (await canLaunch("tel:${contact.number}")) {
                            await launch("tel:${contact.number}");
                          }
                        },
                      ),
                      Text(
                        contact.number,
                        style: GoogleFonts.aBeeZee(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 22),
                      ),
                      IconButton(
                        icon: Icon(Icons.content_copy),
                        color: Colors.white,
                        onPressed: () {
                          Clipboard.setData(
                              ClipboardData(text: "${contact.number}"));
                          Scaffold.of(context).removeCurrentSnackBar();
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: Text("Número copiado"),
                          ));
                        },
                      ),
                    ],
                  )
                ],
              ),
            )));
  }
}
