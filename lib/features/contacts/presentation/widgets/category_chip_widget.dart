import 'package:google_fonts/google_fonts.dart';
import 'package:meta/meta.dart';
import 'package:flutter/material.dart';

class CategoryChipWidget extends StatelessWidget {
  final String category;
  final bool selected;
  final void Function(bool) onSelected;

  const CategoryChipWidget({
    Key key,
    @required this.category,
    @required this.selected,
    @required this.onSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 4.0, vertical: 0.0),
      child: FilterChip(
        shadowColor: Colors.purple,
        selectedColor: Colors.purple,
        showCheckmark: true,
        checkmarkColor: Colors.white,
        backgroundColor: Colors.grey[400],
        selected: selected,
        padding: EdgeInsets.all(8),
        label: Text(
          category,
          style: GoogleFonts.aBeeZee(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: 18,
          ),
        ),
        onSelected: onSelected,
      ),
    );
  }
}
