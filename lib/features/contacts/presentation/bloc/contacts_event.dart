import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ContactsEvent extends Equatable {
  const ContactsEvent();
}

class GetContactsEvent extends ContactsEvent {
  @override
  final List<Object> props = [];
}
