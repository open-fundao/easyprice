import 'package:portugal_iniciante/features/contacts/domain/entities/contact.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class ContactsState extends Equatable {
  const ContactsState();
}

class Empty extends ContactsState {
  @override
  List<Object> get props => [];
}

class Loading extends ContactsState {
  @override
  List<Object> get props => [];
}

class Loaded extends ContactsState {
  final List<Contact> contacts;

  Loaded({@required this.contacts});

  @override
  List<Object> get props => [this.contacts];
}

class Error extends ContactsState {
  final String message;

  Error({@required this.message});

  @override
  List<Object> get props => [this.message];
}
