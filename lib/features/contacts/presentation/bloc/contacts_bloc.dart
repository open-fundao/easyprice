import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:portugal_iniciante/features/contacts/domain/usecases/get_contacts.dart';
import 'package:flutter/widgets.dart';

import 'bloc.dart';
import 'package:meta/meta.dart';

class ContactsBloc extends Bloc<ContactsEvent, ContactsState>
    with ChangeNotifier {
  final GetContacts getContacts;

  ContactsBloc({@required this.getContacts}) : assert(getContacts != null);

  @override
  ContactsState get initialState => Empty();

  @override
  Stream<ContactsState> mapEventToState(
    ContactsEvent event,
  ) async* {
    if (event is GetContactsEvent) {
      final contactsOrFailure = getContacts();
      notifyListeners();
      yield Loading();
      final contacts = await contactsOrFailure;

      notifyListeners();
      yield* contacts.fold((failure) async* {
        yield Error(message: "Failed to fetch contacts");
      }, (contacts) async* {
        yield Loaded(contacts: contacts);
      });
    }
  }
}
