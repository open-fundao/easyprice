import 'package:after_layout/after_layout.dart';
import 'package:portugal_iniciante/features/contacts/data/models/contact_model.dart';
import 'package:portugal_iniciante/features/contacts/presentation/bloc/bloc.dart';
import 'package:portugal_iniciante/features/contacts/presentation/widgets/category_chip_widget.dart';
import 'package:portugal_iniciante/features/contacts/presentation/widgets/contact_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class ContactsPage extends StatefulWidget {
  ContactsPage({Key key}) : super(key: key);

  @override
  _ContactsPageState createState() => _ContactsPageState();
}

class _ContactsPageState extends State<ContactsPage>
    with AfterLayoutMixin<ContactsPage> {
  ContactsState state;

  final categories = <String>[];
  List<ContactModel> contacts = <ContactModel>[];
  List<ContactModel> _filtered = <ContactModel>[];

  @override
  void initState() {
    super.initState();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    Provider.of<ContactsBloc>(context, listen: false).add(GetContactsEvent());
  }

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        backgroundColor: Theme.of(context).backgroundColor,
        actionsIconTheme: IconThemeData(color: Colors.black),
        iconTheme: IconThemeData(color: Colors.black),
        elevation: 0,
        title: Text("Contatos Importantes",
            style: GoogleFonts.aBeeZee(
                fontSize: 20,
                color: Colors.black,
                fontWeight: FontWeight.w900)),
        actions: <Widget>[],
      ),
      body: Consumer<ContactsBloc>(
        builder: (_, bloc, child) {
          final blocState = bloc.state;
          if (blocState is Empty) print("Is empty");
          if (blocState is Error) print("Is error: ${blocState.message}");
          if (blocState is Loading) {}

          if (blocState is Loaded && blocState != state) {
            state = blocState;
            this.contacts = blocState.contacts;
            this._filtered.addAll(this.contacts);
            this.categories.addAll(
                blocState.contacts.map((contact) => contact.category).toSet());
          }

          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: double.infinity,
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: categories.map<Widget>((category) {
                        return CategoryChipWidget(
                          category: category,
                          selected: _filtered
                              .every((contact) => contact.category == category),
                          onSelected: (bool value) =>
                              _onFilterContacts(category),
                        );
                      }).toList(growable: false),
                    ),
                  ),
                ),
              ),
              Flexible(
                child: blocState is Loading
                    ? Center(
                        child: RefreshProgressIndicator(
                        valueColor:
                            AlwaysStoppedAnimation<Color>(Colors.redAccent),
                      ))
                    : ListView.builder(
                        itemCount: _filtered.length,
                        itemBuilder: (context, index) =>
                            ContactWidget(contact: _filtered[index]),
                      ),
              )
            ],
          );
        },
      ),
    );
  }

  void _onFilterContacts(String category) {
    setState(() {
      _filtered = this
          .contacts
          .where((contact) => contact.category == category)
          .toList();
    });
  }
}
