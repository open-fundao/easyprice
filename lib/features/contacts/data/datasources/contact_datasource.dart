import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:portugal_iniciante/core/error/exception.dart';
import 'package:portugal_iniciante/features/contacts/data/models/contact_model.dart';
import 'package:flutter/widgets.dart';
import 'package:meta/meta.dart';

abstract class IContactDataSource {
  /// Calls remote endpoint
  ///
  /// Throws a [ServerException] for all error codes
  Future<List<ContactModel>> getContacts();
}

class ContactDataSourceFirestore implements IContactDataSource {
  final FirebaseFirestore firestore;

  ContactDataSourceFirestore({@required this.firestore});

  @override
  Future<List<ContactModel>> getContacts() async {
    return Future(() async {
      final List<ContactModel> contacts = [];
      try {
        final documents = await firestore.collection("contacts").get();

        contacts.addAll(documents.docs
            .map((document) => ContactModel.fromJson(document.data())));
      } catch (exception) {
        throw ServerException();
      }

      return contacts;
    });
  }
}
