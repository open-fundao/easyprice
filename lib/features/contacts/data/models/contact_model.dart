import 'package:portugal_iniciante/features/contacts/domain/entities/contact.dart';
import 'package:flutter/foundation.dart';
import 'package:meta/meta.dart';

class ContactModel extends Contact {
  final String category;
  final String title;
  final String description;
  final String number;

  ContactModel({
    @required this.category,
    @required this.title,
    @required this.description,
    @required this.number,
  }) : super(
          category: category,
          title: title,
          description: description,
          number: number,
        );

  factory ContactModel.fromJson(Map<String, dynamic> json) {
    return ContactModel(
      category: json['category'],
      description: json['description'],
      number: json['number'],
      title: json['title'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "category": category,
      "title": title,
      "description": description,
      "number": number
    };
  }
}
