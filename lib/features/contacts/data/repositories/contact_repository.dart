import 'package:dartz/dartz.dart';
import 'package:portugal_iniciante/core/error/exception.dart';
import 'package:portugal_iniciante/core/error/failure.dart';
import 'package:portugal_iniciante/features/contacts/data/datasources/contact_datasource.dart';
import 'package:portugal_iniciante/features/contacts/domain/entities/contact.dart';
import 'package:portugal_iniciante/features/contacts/domain/repositories/contact_repository.dart';

import 'package:meta/meta.dart';

class ContactRepository implements IContactsRepository {
  final IContactDataSource dataSource;

  ContactRepository({@required this.dataSource}) : super();

  @override
  Future<Either<Failure, List<Contact>>> getContacts() async {
    try {
      var contacts = await dataSource.getContacts();
      return Right(contacts);
    } on ServerException {
      return Left(ServerFailure());
    }
  }
}
