import 'package:portugal_iniciante/features/groceries/model/grocery_list.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

class MercadoPage extends StatefulWidget {
  const MercadoPage({Key key}) : super(key: key);

  @override
  _MercadoPageState createState() => _MercadoPageState();
}

class _MercadoPageState extends State<MercadoPage>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  int _currentPageIndex;
  Map<int, List<Widget>> _pages = {
    0: [ListaAtivaMercado(), ListaArquivadaMercado()],
    1: [ListaPrecoMercado(), ListaPrecoCadastradoMercado()]
  };

  Map<int, List<Widget>> _tabs = {
    0: [
      Tab(icon: Icon(Icons.list), text: "Ativas"),
      Tab(icon: Icon(Icons.archive), text: "Arquivadas")
    ],
    1: [
      Tab(icon: Icon(Icons.euro_symbol), text: "Recentes"),
      Tab(icon: Icon(Icons.save), text: "Cadastrados")
    ],
  };

  final _novaListaFormKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    _currentPageIndex = 0;
    _tabController = TabController(length: 2, vsync: this, initialIndex: 0);
    _tabController.addListener(_onTabChanged);
  }

  @override
  void dispose() {
    _tabController.removeListener(_onTabChanged);
    _tabController.dispose();
    super.dispose();
  }

  void onTapMenu(int newIndex) {
    setState(() {
      _currentPageIndex = newIndex;
    });
  }

  void _onTabChanged() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Mercado"),
        centerTitle: true,
        bottom:
            TabBar(controller: _tabController, tabs: _tabs[_currentPageIndex]),
      ),
      body: TabBarView(
        physics: NeverScrollableScrollPhysics(),
        controller: _tabController,
        children: _pages[_currentPageIndex],
      ),
      bottomNavigationBar: Container(
        child: BottomNavigationBar(
          selectedItemColor: Theme.of(context).primaryColor,
          unselectedItemColor: Theme.of(context).primaryColor.withOpacity(0.5),
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.list),
              title: Text("Listas"),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.euro_symbol),
              title: Text("Preços"),
            ),
          ],
          onTap: onTapMenu,
          currentIndex: _currentPageIndex,
        ),
      ),
      floatingActionButton: _currentPageIndex == 0
          ? FloatingActionButton.extended(
              label: Text("Criar lista"),
              icon: Icon(Icons.add),
              onPressed: () async => await _buildCriaListaDialog(context),
            )
          : FloatingActionButton.extended(
              label: Text("Adicionar preço"),
              icon: Icon(Icons.euro_symbol),
              onPressed: () async => await _buildNovoPrecoDialog(context),
            ),
    );
  }

  Future _buildNovoPrecoDialog(BuildContext context) {
    return showDialog(
        context: context,
        child: SimpleDialog(
          title: const Text("Adicionar novo preço"),
          children: <Widget>[
            SimpleDialogOption(
              onPressed: () {},
              child: const Text("data"),
            )
          ],
        ));
  }

  Future _buildCriaListaDialog(BuildContext context) {
    return showDialog(
        context: context,
        child: SimpleDialog(
          title: const Text(
            "Criar uma nova lista",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          children: <Widget>[
            SimpleDialogOption(
                onPressed: () {},
                child: Container(
                  child: Form(
                    key: _novaListaFormKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        TextFormField(
                          decoration: const InputDecoration(
                              hintText: "Dê um nome para lista"),
                          validator: (value) => value.isEmpty
                              ? "O nome da lista não pode ficar em branco"
                              : null,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 16.0),
                          child: RaisedButton(
                            onPressed: () {
                              if (_novaListaFormKey.currentState.validate()) {
                                // Process data.
                              }
                            },
                            child: Text('Criar'),
                          ),
                        ),
                      ],
                    ),
                  ),
                ))
          ],
        ));
  }
}

class ListaAtivaMercado extends StatelessWidget {
  final listas = List<GroceryList>.generate(
      10000, (i) => GroceryList(title: "Item $i", totalItems: 23));

  ListaAtivaMercado({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        itemCount: listas.length,
        itemBuilder: (context, index) {
          final item = listas[index];
          return Dismissible(
            background: Container(
                child: Row(
              children: <Widget>[
                IconButton(icon: Icon(Icons.delete_forever), onPressed: () {}),
              ],
            )),
            child: Card(
              elevation: 4,
              child: ListTile(
                title: Text(
                  item.title,
                  style: TextStyle(fontWeight: FontWeight.w500),
                ),
                subtitle: Text("${item.totalItems} items"),
                trailing: Icon(Icons.keyboard_arrow_right),
                onTap: () => Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) =>
                        ListaPrecoMercadoPage(title: item.title),
                  ),
                ),
              ),
            ),
            key: ObjectKey(item),
          );
        },
      ),
    );
  }
}

class ListaArquivadaMercado extends StatelessWidget {
  ListaArquivadaMercado({Key key}) : super(key: key);

  final listas = List<String>.generate(10000, (i) => "Item $i");

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        itemCount: 10,
        itemBuilder: (context, index) {
          return Card(
            elevation: 4,
            child: ListTile(
              title: Text(
                "Festa Aniversário",
                style: TextStyle(fontWeight: FontWeight.w500),
              ),
              subtitle: Text("35 itens"),
              trailing: Icon(Icons.keyboard_arrow_right),
              onTap: () {},
            ),
          );
        },
      ),
    );
  }
}

class ListaPrecoMercado extends StatelessWidget {
  const ListaPrecoMercado({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(child: Text("Lista de preços")),
    );
  }
}

class ListaPrecoCadastradoMercado extends StatelessWidget {
  const ListaPrecoCadastradoMercado({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(child: Text("Meus preços cadastrados")),
    );
  }
}

class ListaPrecoMercadoPage extends StatefulWidget {
  final String title;

  ListaPrecoMercadoPage({Key key, @required this.title}) : super(key: key);

  @override
  _ListaPrecoMercadoPageState createState() => _ListaPrecoMercadoPageState();
}

class _ListaPrecoMercadoPageState extends State<ListaPrecoMercadoPage> {
  List<Widget> list;

  @override
  void initState() {
    super.initState();
    list = List.generate(
      10,
      (index) => ListTile(
        key: ValueKey("Product $index"),
        title: Text(
          "Product $index",
          style: TextStyle(fontWeight: FontWeight.w500),
        ),
        subtitle: Text("35 itens"),
        trailing: Icon(Icons.keyboard_arrow_right),
        onTap: () {},
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        child: ReorderableListView(
          children: list,
          onReorder: _onReorder,
        ),
      ),
    );
  }

  void _onReorder(int oldIndex, int newIndex) {
    if (newIndex > oldIndex) {
      newIndex -= 1;
    }
    setState(() {
      final item = list.removeAt(oldIndex);
      list.insert(newIndex, item);
    });
  }
}
