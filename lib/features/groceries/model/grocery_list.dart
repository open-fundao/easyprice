import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

class GroceryList extends Equatable {
  final String title;
  final int totalItems;

  GroceryList({@required this.title, @required this.totalItems});

  @override
  List<Object> get props => [title, totalItems];
}
